﻿using System;
using System.Collections.Generic;
using System.Net;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.LanguageService;
using Floxdc.Ming.Bot.Model.Preferences;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace MingBot.Tests
{
    public class LanguageServiceTests : IClassFixture<ServiceFixture>
    {
        public LanguageServiceTests(ServiceFixture fixture)
        {
            _fixture = fixture;

            _codices = new List<MingCodex>
            {
                new MingCodex
                {
                    Language = "es",
                    Dictionaries = new List<DictionaryInfo>
                    {
                        new DictionaryInfo
                        {
                            Description = "description",
                            Input = "en",
                            Origin = "es",
                            Output = "ru"
                        }
                    }
                }
            };
        }


        [Fact]
        public void GetAvailableLanguages_ShouldReturnCodexWhenUrlIsCorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject(_codices));
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            var result = service.GetAvailableLanguages();

            Assert.Collection(result, pair => Assert.Equal(new KeyValuePair<string, string>("es", "Español (Spanish)"), pair));
        }


        [Fact]
        public void GetAvailableLanguages_ShouldFailWhenResponsedJsonIsIncorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject("bad response"));

            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<JsonException>(() => service.GetAvailableLanguages());
        }


        [Fact]
        public void GetAvailableLanguages_ShouldFailNullWhenWebClientThrowsAnEsception()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Throws(new HttpListenerException());
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<ArgumentNullException>(() => service.GetAvailableLanguages());
        }


        [Fact]
        public void GetAvailableInputLanguages_ShouldReturnCodexWhenUrlIsCorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject(_codices));
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            var result = service.GetAvailableInputLanguages("es");

            Assert.Collection(result, pair => Assert.Equal(new KeyValuePair<string, string>("en", "English"), pair));
        }


        [Fact]
        public void GetAvailableInputLanguages_ShouldFailWhenResponsedJsonIsIncorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject("bad response"));

            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<JsonException>(() => service.GetAvailableInputLanguages("es"));
        }


        [Fact]
        public void GetAvailableInputLanguages_ShouldFailNullWhenWebClientThrowsAnEsception()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Throws(new HttpListenerException());
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<ArgumentNullException>(() => service.GetAvailableInputLanguages("es"));
        }


        [Fact]
        public void GetAvailableOutputLanguages_ShouldReturnCodexWhenUrlIsCorrect()
        {
            var codices = new List<MingCodex>
            {
                new MingCodex
                {
                    Language = "es",
                    Dictionaries = new List<DictionaryInfo>
                    {
                        new DictionaryInfo
                        {
                            Description = "",
                            Input = "en",
                            Origin = "es",
                            Output = "ru"
                        }
                    }
                }
            };

            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject(codices));
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            var result = service.GetAvailableOutputLanguages("es", "en");

            Assert.Collection(result, pair => Assert.Equal(new KeyValuePair<string, string>("ru", "Русский (Russian)"), pair));
        }


        [Fact]
        public void GetAvailableOutputLanguages_ShouldReturnCodexWhenUrlIsCorrectAndDescriptionIsNotEmpty()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject(_codices));
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            var result = service.GetAvailableOutputLanguages("es", "en");

            Assert.Collection(result, pair => Assert.Equal(new KeyValuePair<string, string>("ru+description", "Русский (Russian) description"), pair));
        }


        [Fact]
        public void GetAvailableOutputLanguages_ShouldFailWhenResponsedJsonIsIncorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject("bad response"));

            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<JsonException>(() => service.GetAvailableOutputLanguages("es", "en"));
        }


        [Fact]
        public void GetAvailableOutputLanguages_ShouldFailNullWhenWebClientThrowsAnEsception()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Throws(new HttpListenerException());
            var service = new LanguageService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<ArgumentNullException>(() => service.GetAvailableOutputLanguages("es", "en"));
        }


        private readonly ServiceFixture _fixture;
        private readonly List<MingCodex> _codices;
    }
}
