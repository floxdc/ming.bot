﻿using System;
using Floxdc.Ming.Bot.Infrastructure;
using Moq;

namespace MingBot.Tests
{
    public class ServiceFixture : IDisposable
    {
        public ServiceFixture()
        {
            var cacheMock = new Mock<ICache>();
            cacheMock.Setup(c => c.Get(It.IsAny<string>())).Returns(false);
            Cache = cacheMock.Object;

            Retrier = new Retrier<string>();
        }


        public void Dispose() { }
        

        public ICache Cache { get; }
        public Retrier<string> Retrier { get; }
    }
}
