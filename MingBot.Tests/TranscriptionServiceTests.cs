﻿using System;
using System.Net;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.Preferences;
using Floxdc.Ming.Bot.Model.Transcription;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace MingBot.Tests
{
    public class TranscriptionServiceTests : IClassFixture<ServiceFixture>
    {
        public TranscriptionServiceTests(ServiceFixture fixture)
        {
            _fixture = fixture;

            _dictionaryInfo = new DictionaryInfo
            {
                Description = "description",
                Input = "en",
                Origin = "en",
                Output = "ru"
            };
        }

        
        [Fact]
        public void GetTranscription_ShouldTranscribeWhenUrlIsCorrect()
        {
            var response = new TranscriptionResult
            {
                Content = "response",
                IsTranscribed = true
            };

            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject(response));
            var service = new TranscriptionService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            var result = service.GetTranscription(_dictionaryInfo, "request");

            Assert.IsType(typeof(TranscriptionResult), result);
            Assert.True(result.IsTranscribed);
            Assert.Equal(result.Content, response.Content);
        }


        [Fact]
        public void GetTranscription_ShouldFailWhenResponsedJsonIsIncorrect()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Returns(JsonConvert.SerializeObject("bad response"));
            var service = new TranscriptionService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<JsonException>(() => service.GetTranscription(_dictionaryInfo, "request"));
        }


        [Fact]
        public void GetTranscription_ShouldFailNullWhenWebClientThrowsAnEsception()
        {
            var clientMock = new Mock<IWebClient>();
            clientMock.Setup(c => c.Get(It.IsAny<string>())).Throws(new HttpListenerException());
            var service = new TranscriptionService(_fixture.Cache, _fixture.Retrier, clientMock.Object);

            Assert.ThrowsAny<ArgumentNullException>(() => service.GetTranscription(_dictionaryInfo, "request"));
        }


        private readonly DictionaryInfo _dictionaryInfo;
        private readonly ServiceFixture _fixture;
    }
}
