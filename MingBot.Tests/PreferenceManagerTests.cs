﻿using System.Threading.Tasks;
using Floxdc.Ming.Bot.Model.Preferences;
using Floxdc.Ming.Bot.Model.Repository;
using Microsoft.Bot.Builder.Dialogs;
using Moq;
using Newtonsoft.Json;
using Xunit;

namespace MingBot.Tests
{
    public class PreferenceManagerTests
    {
        public PreferenceManagerTests()
        {
            object dummy;
            var contextMock = new Mock<IDialogContext>();
            contextMock.Setup(c => c.UserData.TryGetValue(It.IsAny<string>(), out dummy)).Returns(false);
            _context = contextMock.Object;
        }


        [Fact]
        public async void ArePreferencesSet_ShouldReturnResultWhenTheeraIsAConnectionToTheDb()
        {
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.ArePreferencesStored(It.IsAny<string>())).Returns(Task.FromResult(true));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.ArePreferencesSet(_context);

            Assert.True(result);
        }


        [Fact]
        public async void ArePreferencesSet_ShouldReturnFalseResultWhenTheeraIsNoConnectionToTheDb()
        {
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.ArePreferencesStored(It.IsAny<string>())).Returns(Task.FromResult(false));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.ArePreferencesSet(_context);

            Assert.False(result);
        }


        [Fact]
        public async void GetPreferences_ShouldReturnResultWhenTheeraIsAConnectionToTheDb()
        {
            var dictionaryInfo = new DictionaryInfo
            {
                Description = "description",
                Input = "en",
                Origin = "es",
                Output = "ru"
            };
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.GetPreferences(It.IsAny<string>()))
                .Returns(Task.FromResult(JsonConvert.SerializeObject(dictionaryInfo)));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.GetPreferenses(_context);

            Assert.IsType(typeof(DictionaryInfo), result);
            Assert.Equal(dictionaryInfo.Description, result.Description);
            Assert.Equal(dictionaryInfo.Input, result.Input);
            Assert.Equal(dictionaryInfo.Origin, result.Origin);
            Assert.Equal(dictionaryInfo.Output, result.Output);
        }


        [Fact]
        public async void GetPreferences_ShouldHandleEmptyResultWhenTheeraIsNoConnectionToTheDb()
        {
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.GetPreferences(It.IsAny<string>()))
                .Returns(Task.FromResult(JsonConvert.SerializeObject(null)));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.GetPreferenses(_context);

              Assert.IsType(typeof(DictionaryInfo), result);
        }


        [Fact]
        public async void SetPreferences_ShouldReturnResultWhenTheeraIsAConnectionToTheDb()
        {
            var preferences = new DictionaryPreferences
            {
                InputLanguage = "en",
                TextLanguage = "es",
                OutputLanguage = "ru"
            };
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.SetPreferences(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.SetPreferences(_context, preferences);

            Assert.True(result);
        }


        [Fact]
        public async void SetPreferences_ShouldHandleEmptyResultWhenTheeraIsNoConnectionToTheDb()
        {
            var preferences = new DictionaryPreferences
            {
                InputLanguage = "en",
                TextLanguage = null,
                OutputLanguage = "ru"
            };
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.SetPreferences(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(true));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.SetPreferences(_context, preferences);

            Assert.False(result);
        }


        [Fact]
        public async void SetPreferences_ShouldHandleEmptyResultWhenTheeraIsAnErrorInPreferences()
        {
            var preferences = new DictionaryPreferences
            {
                InputLanguage = "en",
                TextLanguage = null,
                OutputLanguage = "ru"
            };
            var repoMock = new Mock<IRepository>();
            repoMock.Setup(r => r.SetPreferences(It.IsAny<string>(), It.IsAny<string>()))
                .Returns(Task.FromResult(false));
            var manager = new PreferenceManager(repoMock.Object);

            var result = await manager.SetPreferences(_context, preferences);

            Assert.False(result);
        }


        private readonly IDialogContext _context;
    }
}
