﻿using Microsoft.Bot.Builder.Dialogs.Internals;
using Microsoft.Bot.Connector;

namespace Floxdc.Ming.Bot.Infrastructure
{
    internal class Replier
    {
        internal static Activity GetMessageReply(IActivity activity, string message = null)
            => new Activity
            {
                Text = message,
                Id = activity.Id,
                Type = ActivityTypes.Message
            };


        internal static Activity GetTypingReply(IActivity activity)
            => new Activity
            {
                Id = activity.Id,
                Type = ActivityTypes.Typing
            };


        internal static IMessageActivity GetTypingReply(IBotToUser context)
        {
            var reply = context.MakeMessage();
            reply.Type = ActivityTypes.Typing;

            return reply;
        }
    }
}
