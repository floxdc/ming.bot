﻿using System;
using System.Runtime.Caching;

namespace Floxdc.Ming.Bot.Infrastructure
{
    public class Cache : ICache
    {
        public Cache()
        {
            _cache = MemoryCache.Default;
        }


        public object Get(string key) 
            => _cache.Get(key);


        public void Set(string key, object value)
            => _cache.Set(key, value, new CacheItemPolicy {AbsoluteExpiration = DateTimeOffset.UtcNow.AddMinutes(30)});


        private readonly MemoryCache _cache;
    }
}