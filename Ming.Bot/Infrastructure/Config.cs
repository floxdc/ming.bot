﻿using System.Runtime.Caching;
using System.Web.Configuration;

namespace Floxdc.Ming.Bot.Infrastructure
{
    internal static class Config
    {
        internal static int NumberOfRetries 
            => int.Parse(WebConfigurationManager.AppSettings["NumberOfRetries"]);


        internal static int RetryTimeout
            => int.Parse(WebConfigurationManager.AppSettings["RetryTimeoutInMilliseconds"]);


        internal static int RetryTimeoutSql
            => int.Parse(WebConfigurationManager.AppSettings["SqlRetryTimeoutInMilliseconds"]);


        internal static class Api
        {
            internal static string BaseUrl
                => WebConfigurationManager.AppSettings["BaseApiAddress"];


            internal static string ContentType
                => WebConfigurationManager.AppSettings["ApiContentType"];


            internal static string DictionariesUrl
                => "dictionaries";


            internal static string TranscribeUrl
                => "transcribe";
        }


        internal static class Keys
        {
            internal const string DictionaryInfo = "DictionaryInfo";
            internal const string UserId = "UserId";
            internal const char OutputDescriptionDivider = '+';
        }


        internal static class Db
        {
            internal static string ConnectionString
               => WebConfigurationManager.ConnectionStrings["Nagoya"].ToString();

            internal static string Key
               => WebConfigurationManager.AppSettings["AppRoleKey"];

            internal static string Role
               => WebConfigurationManager.AppSettings["AppRole"];
        }


        private static readonly MemoryCache Cache = MemoryCache.Default;
    }
}
