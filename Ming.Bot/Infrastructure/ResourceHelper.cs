﻿using System;

namespace Floxdc.Ming.Bot.Infrastructure
{
    internal static class ResourceHelper
    {
        internal static string PickOne(this string source)
        {
            source = source.TrimStart('"').TrimEnd('"');
            var array = source.Split(new[]{"\", \""}, StringSplitOptions.RemoveEmptyEntries);
            int value;

            lock (SyncLock)
                value = Random.Next(0, array.Length - 1);
            
            return value < 0
                ? array[0] 
                : array[value];
        }


        private static readonly Random Random = new Random();
        private static readonly object SyncLock = new object();
    }
}