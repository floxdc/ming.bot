﻿using System;
using NLog;

namespace Floxdc.Ming.Bot.Infrastructure
{
    public class Retrier<TResult>
    {
        internal TResult Try(Func<TResult> func, int numberOfRetries)
        {
            var retryCount = numberOfRetries;

            while (retryCount > 0)
            {
                try
                {
                    return func();
                }
                catch (Exception ex)
                {
                    LogException(ex, func.Method.Name, retryCount);
                }

                retryCount--;
            }

            return default(TResult);
        }


        public TResult TryWithDelay(Func<TResult> func, int numberOfRetries, int delayInMilliseconds)
        {
            var retryCount = numberOfRetries;

            while (retryCount > 0)
            {
                try
                {
                    return func();
                }
                catch (Exception ex)
                {
                    LogException(ex, func.Method.Name, retryCount);
                }

                retryCount--;
                System.Threading.Thread.Sleep(delayInMilliseconds);
            }

            return default(TResult);
        }


        private void LogException(Exception ex, string methodName, int retryCount)
        {
            if (retryCount > 1)
            {
                _logger.Warn(ex, "{1}: {0}", ex.Message, methodName);
            }
            else
            {
                _logger.Error(ex, "{1}: {0}", ex.Message, methodName);
            }
        }


        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
    }
}
