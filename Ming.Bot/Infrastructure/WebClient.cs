﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace Floxdc.Ming.Bot.Infrastructure
{
    public class WebClient : IWebClient
    {
        public string Get(string url) 
            => GetResponseAsync(url).Result;


        private HttpClient GetClient()
        {
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(Config.Api.ContentType));

            return client;
        }


        private async Task<string> GetResponseAsync(string url)
        {
            using (var client = GetClient())
            {
                var uri = new Uri(Config.Api.BaseUrl + url);
                var response = await client.GetAsync(uri).ConfigureAwait(false);
                return await response.Content.ReadAsStringAsync();
            }
        }
    }
}