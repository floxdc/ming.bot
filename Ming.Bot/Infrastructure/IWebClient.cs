namespace Floxdc.Ming.Bot.Infrastructure
{
    public interface IWebClient
    {
        string Get(string url);
    }
}