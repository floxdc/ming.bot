﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.Preferences;
using Floxdc.Ming.Bot.Model.Repository;
using Floxdc.Ming.Bot.Model.Transcription;
using Floxdc.Ming.Bot.Resource;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;

namespace Floxdc.Ming.Bot.Model
{
    [Serializable]
    public sealed class MingDialog : IDialog<MingDialog>
    {
        public MingDialog()
        {
            var repository = new SqlRepository();
            _preferenceManager = new PreferenceManager(repository);
        }


        public async Task StartAsync(IDialogContext context)
        {
            context.Wait(MessageReceivedAsync);
            await Task.FromResult(0);
        }


        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> awaitable)
        {
            var message = await awaitable;

            _lastMessage = message.Text;
            context.UserData.SetValue(Config.Keys.UserId, message.From.Id);

            if (await ArePreferencesSet(context, message))
                await Transcribe(context, message.Text);
            else
                GetDictionaryPreferences(context);
        }


        #region private

        private async Task<bool> ArePreferencesSet(IDialogContext context, IMessageActivity activity)
            => !OptionsCommandTexts.Contains(activity.Text.ToLower())
               && await _preferenceManager.ArePreferencesSet(context);


        private void GetDictionaryPreferences(IDialogContext context)
        {
            var preferences = new FormDialog<DictionaryPreferences>(new DictionaryPreferences(),
                DictionaryPreferences.Build, FormOptions.PromptInStart);
            context.Call(preferences, SetDictionaryPreferences);
        }


        private async Task SetDictionaryPreferences(IDialogContext context, IAwaitable<DictionaryPreferences> awaitable)
        {
            var preferences = await awaitable;
            if (preferences == null)
                context.Wait(MessageReceivedAsync);

            var isSucceed = await _preferenceManager.SetPreferences(context, preferences);
            if (!isSucceed)
            {
                await context.PostAsync(Floxdc_Ming_Bot_Model_MingDialog.Message_UnknownErrorWhileSettingThePreferences);
                context.Wait(MessageReceivedAsync);
            }

            await context.PostAsync(Floxdc_Ming_Bot_Model_MingDialog.Message_PreferencesAreSet);

            if (_lastMessage.StartsWith("/"))
                context.Wait(MessageReceivedAsync);
            else
                await Transcribe(context, _lastMessage);
        }


        private async Task Transcribe(IDialogContext context, string text)
        {
            var dictionaryInfo = await _preferenceManager.GetPreferenses(context);
            var cache = new Cache();
            var client = new WebClient();
            var retrier = new Retrier<string>();
            var transcriptionService = new TranscriptionService(cache, retrier, client);

            var responce = transcriptionService.GetTranscription(dictionaryInfo, text);
            var reply = responce.Content;
            if (!responce.IsTranscribed)
                reply = string.Format(Floxdc_Ming_Bot_Model_MingDialog.Message_CantTranslate, reply);

            await context.PostAsync(reply);

            context.Wait(MessageReceivedAsync);
        }

        #endregion


        private string _lastMessage = string.Empty;

        [NonSerialized]
        private static readonly List<string> OptionsCommandTexts = new List<string>
        {
            "/opt",
            "/opts",
            "/options",
            "/настройки"
        };
        private readonly PreferenceManager _preferenceManager;
    }
}