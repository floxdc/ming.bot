﻿using System.Collections.Generic;

namespace Floxdc.Ming.Bot.Model.LanguageService
{
    public interface ILanguageService
    {
        IEnumerable<KeyValuePair<string, string>> GetAvailableInputLanguages(string textLanguage);
        IEnumerable<KeyValuePair<string, string>> GetAvailableLanguages();
        IEnumerable<KeyValuePair<string, string>> GetAvailableOutputLanguages(string textLanguage, string inputLanguage);
    }
}