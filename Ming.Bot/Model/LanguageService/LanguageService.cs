﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.Preferences;
using Newtonsoft.Json;
using NLog;

namespace Floxdc.Ming.Bot.Model.LanguageService
{
    [Serializable]
    public class LanguageService : ILanguageService
    {
        public LanguageService(ICache cache, Retrier<string> retrier, IWebClient client)
        {
            _cache = cache;
            _client = client;
            _retrier = retrier;
        }


        public IEnumerable<KeyValuePair<string, string>> GetAvailableInputLanguages(string textLanguage)
            => GetDictionaries()
                .First(d => d.Language == textLanguage)
                .Dictionaries.Select(d => GetLanguageDescription(d.Input, string.Empty));


        public IEnumerable<KeyValuePair<string, string>> GetAvailableLanguages()
            => GetDictionaries()
                .Select(d => GetLanguageDescription(d.Language, string.Empty));


        public IEnumerable<KeyValuePair<string, string>> GetAvailableOutputLanguages(string textLanguage, string inputLanguage)
            => GetDictionaries()
                .First(d => d.Language == textLanguage)
                .Dictionaries.Where(d => d.Input == inputLanguage)
                .Select(d => GetLanguageDescription(d.Output, d.Description));


        private IEnumerable<MingCodex> GetDictionaries() 
            => _cache.Get(AvailableDictionaries) as IEnumerable<MingCodex> ?? RequestDictionaries();


        private IEnumerable<MingCodex> RequestDictionaries()
        {
            var json = _retrier.TryWithDelay(() => _client.Get(Config.Api.DictionariesUrl), Config.NumberOfRetries, Config.RetryTimeout);

            try
            {
                var dictionaries = JsonConvert.DeserializeObject<List<MingCodex>>(json);
                _cache.Set(AvailableDictionaries, dictionaries);
                return dictionaries;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
                throw;
            }
        }


        private static KeyValuePair<string, string> GetLanguageDescription(string langCode, string dictionaryDescription = "")
        {
            var culture = new CultureInfo(langCode);

            var cultureName = culture.Name;
            var cultureDescription = string.Compare(culture.EnglishName, culture.NativeName, StringComparison.CurrentCulture) == 0
                ? culture.EnglishName
                : $"{culture.NativeName.Substring(0, 1).ToUpper() + culture.NativeName.Substring(1)} ({culture.EnglishName})";

            if (string.IsNullOrWhiteSpace(dictionaryDescription))
                return new KeyValuePair<string, string>(cultureName, cultureDescription);

            cultureName += $"{Config.Keys.OutputDescriptionDivider}{dictionaryDescription}";
            cultureDescription += $" {dictionaryDescription}";

            return new KeyValuePair<string, string>(cultureName, cultureDescription);
        }


        private const string AvailableDictionaries = "AvailableDictionaries";
        [NonSerialized]
        private readonly ICache _cache;
        [NonSerialized]
        private readonly IWebClient _client;
        [NonSerialized]
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        [NonSerialized]
        private readonly Retrier<string> _retrier;
    }
}