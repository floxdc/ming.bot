﻿using System;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.Preferences;
using Newtonsoft.Json;
using NLog;

namespace Floxdc.Ming.Bot.Model.Transcription
{
    [Serializable]
    public class TranscriptionService : ITranscriptionService
    {
        public TranscriptionService(ICache cache, Retrier<string> retrier, IWebClient client)
        {
            _cache = cache;
            _retrier = retrier;
            _client = client;
        }


        public TranscriptionResult GetTranscription(IDictionaryInfo dictionaryInfo, string message)
        {
            var url =
                $"{Config.Api.TranscribeUrl}/{dictionaryInfo.Origin}/{dictionaryInfo.Input}/{dictionaryInfo.Output}/{dictionaryInfo.Description}/{message}";

            return _cache.Get(url) as TranscriptionResult ??
                   RequestTranscription(url);
        }


        private TranscriptionResult RequestTranscription(string url)
        {
            var json = _retrier.TryWithDelay(() => _client.Get(url), Config.NumberOfRetries, Config.RetryTimeout);

            try
            {
                var result = JsonConvert.DeserializeObject<TranscriptionResult>(json);
                _cache.Set(url, result);
                return result;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, "{1}: {0}", ex.Message, url);
                throw;
            }
        }


        [NonSerialized]
        private readonly ICache _cache;
        [NonSerialized]
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        [NonSerialized]
        private readonly Retrier<string> _retrier; 
        [NonSerialized]
        private readonly IWebClient _client;
    }
}
