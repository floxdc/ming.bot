﻿using Floxdc.Ming.Bot.Model.Preferences;

namespace Floxdc.Ming.Bot.Model.Transcription
{
    public interface ITranscriptionService
    {
        TranscriptionResult GetTranscription(IDictionaryInfo dictionaryInfo, string message);
    }
}