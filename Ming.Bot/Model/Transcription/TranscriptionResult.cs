﻿namespace Floxdc.Ming.Bot.Model.Transcription
{
    public class TranscriptionResult
    {
        public bool IsTranscribed { get; set; }
        public string Content { get; set; }
    }
}
