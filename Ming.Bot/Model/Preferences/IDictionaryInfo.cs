﻿namespace Floxdc.Ming.Bot.Model.Preferences
{
    public interface IDictionaryInfo
    {
        string Description { get; }
        string Input { get; }
        string Origin { get; }
        string Output { get; }
    }
}
