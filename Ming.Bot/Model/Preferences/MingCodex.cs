﻿using System.Collections.Generic;

namespace Floxdc.Ming.Bot.Model.Preferences
{
    public class MingCodex : IMingCodex
    {
        public string Language { get; set; }
        public IEnumerable<DictionaryInfo> Dictionaries { get; set; }
    }
}
