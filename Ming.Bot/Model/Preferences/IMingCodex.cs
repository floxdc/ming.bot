﻿using System.Collections.Generic;

namespace Floxdc.Ming.Bot.Model.Preferences
{
    public interface IMingCodex
    {
        string Language { get; }
        IEnumerable<DictionaryInfo> Dictionaries { get; }
    }
}