﻿using Newtonsoft.Json;

namespace Floxdc.Ming.Bot.Model.Preferences
{
    public class DictionaryInfo : IDictionaryInfo
    {
        public string Description { get; set; }
        public string Input { get; set; }
        public string Output { get; set; }
        public string Origin { get; set; }
        public override string ToString()
            => JsonConvert.SerializeObject(this);
    }
}
