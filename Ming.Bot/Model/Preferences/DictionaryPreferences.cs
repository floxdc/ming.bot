﻿using System;
using System.Collections.Generic;
using Floxdc.Ming.Bot.Infrastructure;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Builder.FormFlow.Advanced;

#pragma warning disable 1998

namespace Floxdc.Ming.Bot.Model.Preferences
{
    [Serializable]
    public class DictionaryPreferences
    {
        static DictionaryPreferences()
        {
            Build = BuildForm;
        }

        [NonSerialized]
        public static BuildFormDelegate<DictionaryPreferences> Build; 


        private static IForm<DictionaryPreferences> BuildForm()
        {
            var cache = new Cache();
            var client = new WebClient();
            var retrier = new Retrier<string>();
            var languageService = new LanguageService.LanguageService(cache, retrier, client);

            return new FormBuilder<DictionaryPreferences>()
                .Message("Вначале нужно установить кое-какие настройки...")
                .Field(new FieldReflector<DictionaryPreferences>(nameof(TextLanguage))
                    .SetType(null)
                    .SetDefine(async (state, field) =>
                    {
                        var availableLanguages = languageService.GetAvailableLanguages();
                        SetEnumField(field, availableLanguages);
                        return true;
                    }))
                .Field(new FieldReflector<DictionaryPreferences>(nameof(InputLanguage))
                .SetType(null)
                .SetDefine(async (state, field) =>
                {
                    if (state.TextLanguage != null)
                    {
                        var inputs = languageService.GetAvailableInputLanguages(state.TextLanguage);
                        SetEnumField(field, inputs);
                    }
                    return true;
                }))
                .Field(new FieldReflector<DictionaryPreferences>(nameof(OutputLanguage))
                    .SetType(null)
                    .SetDefine(async (state, field) =>
                    {
                        if (state.TextLanguage != null && state.InputLanguage != null)
                        {
                            var outputs = languageService.GetAvailableOutputLanguages(state.TextLanguage, state.InputLanguage);
                            SetEnumField(field, outputs);
                        }
                        return true;
                    }))
                .Build();
        }


        private static void SetEnumField(Field<DictionaryPreferences> field, IEnumerable<KeyValuePair<string, string>> values)
        {
            foreach (var value in values)
                field.AddDescription(value.Key, value.Value)
                    .AddTerms(value.Key, value.Value);
        }


        [Template(TemplateUsage.EnumSelectOne, "На каком языке пользователь введёт слово? {||}", "Какой алфавит ты будешь использовать? {||}", ChoiceStyle = ChoiceStyleOptions.PerLine)]
        public string InputLanguage;

        [Template(TemplateUsage.EnumSelectOne, "На какой язык нужно транскрибировать? {||}", "На каком языке тебе прочесть? {||}", "На каком языке произнести слово? {||}", ChoiceStyle = ChoiceStyleOptions.PerLine)]
        public string OutputLanguage;

        [Template(TemplateUsage.EnumSelectOne, "С какого языка нужно транскрибировать? {||}", "На каком языке будет твой текст? {||}", ChoiceStyle = ChoiceStyleOptions.PerLine)]
        public string TextLanguage;
    }
}
