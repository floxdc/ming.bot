﻿using System;
using System.Threading.Tasks;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model.Repository;
using Microsoft.Bot.Builder.Dialogs;
using Newtonsoft.Json;
using NLog;

namespace Floxdc.Ming.Bot.Model.Preferences
{
    [Serializable]
    internal class PreferenceManager
    {
        internal PreferenceManager(IRepository repository)
        {
            _repository = repository;
        }


        internal async Task<bool> ArePreferencesSet(IDialogContext context)
        {
            object dummy;
            return context.UserData.TryGetValue(Config.Keys.DictionaryInfo, out dummy)
                   || await ArePreferencesStored(context);
        }


        internal async Task<DictionaryInfo> GetPreferenses(IDialogContext context)
        {
            DictionaryInfo preferences;
            var isInMemory = context.UserData.TryGetValue(Config.Keys.DictionaryInfo, out preferences);

            if (isInMemory)
                return preferences;

            try
            {
                var json = await GetPreferencesFromDb(context);
                preferences = JsonConvert.DeserializeObject<DictionaryInfo>(json);
                StorePreferencesToMemory(context, preferences);
            }
            catch (Exception ex)
            {
                await context.PostAsync(Resource.Floxdc_Ming_Bot_Model_Preferences_PreferenceManager.Error_CantExtractPreferences);
                Logger.Error(ex, "{0}", ex.Message);
                throw;
            }

            return preferences;
        }


        internal async Task<bool> SetPreferences(IDialogContext context, DictionaryPreferences preferences)
        {
            var isSucceed = false;
            try
            {
                string description;
                var output = GetOutputLangAndDescription(preferences.OutputLanguage, out description);

                var info = GetDictionaryInfo(preferences.TextLanguage, preferences.InputLanguage, output, description);

                await StorePreferencesToDb(context, info);
                StorePreferencesToMemory(context, info);
                isSucceed = true;
            }
            catch (Exception ex)
            {
                Logger.Error(ex, ex.Message);
            }

            return isSucceed;
        }


        #region private
        
        private async Task<bool> ArePreferencesStored(IDialogContext context)
        {
            var userId = GetUserId(context);
            return await _repository.ArePreferencesStored(userId);
        }


        private static DictionaryInfo GetDictionaryInfo(string origin, string input, string output, string desc)
        {
            if (string.IsNullOrEmpty(origin))
                throw new ArgumentNullException(origin);

            if (string.IsNullOrEmpty(input))
                throw new ArgumentNullException(input);

            if (string.IsNullOrEmpty(output))
                throw new ArgumentNullException(output);

            return new DictionaryInfo
            {
                Description = desc,
                Input = input,
                Origin = origin,
                Output = output
            };
        }


        private string GetOutputLangAndDescription(string target, out string description)
        {
            var array = target.Split(Config.Keys.OutputDescriptionDivider);

            description = (array.Length > 1)
                ? array[1]
                : string.Empty;

            return array[0];
        }


        private async Task<string> GetPreferencesFromDb(IDialogContext context)
        {
            var userId = GetUserId(context);
            return await _repository.GetPreferences(userId);
        }


        private static string GetUserId(IDialogContext context)
        {
            string userId;
            context.UserData.TryGetValue(Config.Keys.UserId, out userId);
            return userId;
        }


        private async Task StorePreferencesToDb(IDialogContext context, DictionaryInfo info)
        {
            var userId = GetUserId(context);
            await _repository.SetPreferences(userId, info.ToString());
        }


        private void StorePreferencesToMemory(IDialogContext context, DictionaryInfo info)
            => context.UserData.SetValue(Config.Keys.DictionaryInfo, info);

        #endregion


        [NonSerialized]
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private readonly IRepository _repository;
    }
}