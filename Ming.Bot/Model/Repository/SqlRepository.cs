﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;
using Floxdc.Ming.Bot.Infrastructure;

namespace Floxdc.Ming.Bot.Model.Repository
{
    [Serializable]
    internal class SqlRepository : IRepository
    {
        public async Task<bool> ArePreferencesStored(string userId)
        {
            var retrier = new Retrier<Task<bool>>();
            return await retrier.TryWithDelay(async () => await AreStored(userId), Config.NumberOfRetries,
                Config.RetryTimeoutSql);
        }


        public async Task<string> GetPreferences(string userId)
        {
            var retrier = new Retrier<Task<string>>();
            return await retrier.TryWithDelay(async () => await Get(userId), Config.NumberOfRetries,
                Config.RetryTimeoutSql);
        }


        public async Task<bool> SetPreferences(string userId, string preferences)
        {
            var retrier = new Retrier<Task<bool>>();
            return await retrier.TryWithDelay(async () => await Set(userId, preferences), Config.NumberOfRetries,
                Config.RetryTimeoutSql);
        }


        #region private

        private static async Task<bool> ArePreferencesExists(SqlConnection connection, string userId)
        {
            bool isExists;

            using (var command = new SqlCommand(IsExists, connection))
            {
                command.Parameters.AddWithValue("@userId", userId);
                isExists = await command.ExecuteScalarAsync() != null;
            }

            return isExists;
        }


        private async Task<bool> AreStored(string userId)
        {
            using (var connection = new SqlConnection(Config.Db.ConnectionString))
            {
                await connection.OpenAsync();
                //AuthorizeApp(connection);
                return await ArePreferencesExists(connection, userId);
            }
        }


        private void AuthorizeApp(SqlConnection connection)
        {
            using (var command = new SqlCommand("sp_setapprole", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@rolename", Config.Db.Role);
                command.Parameters.AddWithValue("@password", Config.Db.Key);
                command.Parameters.AddWithValue("@encrypt", "none");

                command.ExecuteNonQuery();
            }
        }


        private async Task CreatePreferences(SqlConnection connection, string userId, string preferences)
        {
            var command = new SqlCommand(Create, connection);
            command.Parameters.AddWithValue("@created", DateTime.Now);
            command.Parameters.AddWithValue("@content", preferences);
            command.Parameters.AddWithValue("@userId", userId);
            command.Parameters.AddWithValue("@updated", DateTime.Now);

            await command.ExecuteNonQueryAsync();
        }


        private async Task<string> Get(string userId)
        {
            using (var connection = new SqlConnection(Config.Db.ConnectionString))
            {
                await connection.OpenAsync();
                //AuthorizeApp(connection);
                return await SelectPreferences(connection, userId);
            }
        }


        private async Task<string> SelectPreferences(SqlConnection connection, string userId)
        {
            string preferences;
            using (var command = new SqlCommand(Select, connection))
            {
                command.Parameters.AddWithValue("@userId", userId);
                preferences = (string) (await command.ExecuteScalarAsync() ?? string.Empty);
            }

            return preferences;
        }


        private async Task<bool> Set(string userId, string preferences)
        {
            using (var connection = new SqlConnection(Config.Db.ConnectionString))
            {
                await connection.OpenAsync();
                //AuthorizeApp(connection);
                var isExists = await ArePreferencesExists(connection, userId);

                if (isExists)
                    //TODO: handle IsActive toggling
                    await UpdatePreferences(connection, userId, preferences);
                else
                    await CreatePreferences(connection, userId, preferences);
            }

            return true;
        }


        private async Task UpdatePreferences(SqlConnection connection, string userId, string preferences)
        {
            var command = new SqlCommand(Update, connection);
            command.Parameters.AddWithValue("@content", preferences);
            command.Parameters.AddWithValue("@userId", userId);
            command.Parameters.AddWithValue("@updated", DateTime.Now);

            await command.ExecuteNonQueryAsync();
        }

        #endregion


        private const string Create =
            "insert into mingdb.dbo.BotPreferences(UserId, Content, IsActive, Created, Updated) values (@userId, @content, 'True', @created, @updated)";
        private const string IsExists = "select UserId from mingdb.dbo.BotPreferences where UserId = @userId and IsActive = 'True'";
        private const string Select = "select Content from mingdb.dbo.BotPreferences where UserId = @userId and IsActive = 'True'";
        private const string Update = "update mingdb.dbo.BotPreferences set Content = @content, Updated = @updated where UserId = @userId";
    }
}