using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Floxdc.Ming.Bot.Model.Repository
{
    [XmlInclude(typeof(SqlRepository))]
    public interface IRepository
    {
        Task<bool> ArePreferencesStored(string userId);
        Task<string> GetPreferences(string userId);
        Task<bool> SetPreferences(string userId, string preferences);
    }
}