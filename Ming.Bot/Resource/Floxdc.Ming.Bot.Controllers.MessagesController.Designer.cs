﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Floxdc.Ming.Bot.Resource {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Floxdc_Ming_Bot_Controllers_MessagesController {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Floxdc_Ming_Bot_Controllers_MessagesController() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Floxdc.Ming.Bot.Resource.Floxdc.Ming.Bot.Controllers.MessagesController", typeof(Floxdc_Ming_Bot_Controllers_MessagesController).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to You quit on {0} — maybe you can finish next time!.
        /// </summary>
        internal static string Critical_FormCanceled {
            get {
                return ResourceManager.GetString("Critical_FormCanceled", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorry, I&apos;ve had a short circuit.  Please try again..
        /// </summary>
        internal static string Critical_FormCanceledWithException {
            get {
                return ResourceManager.GetString("Critical_FormCanceledWithException", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Unknown activity type ignored: {0}.
        /// </summary>
        internal static string Critical_UnknownActivityType {
            get {
                return ResourceManager.GetString("Critical_UnknownActivityType", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to The data has been deleted.
        /// </summary>
        internal static string Message_DataDeleted {
            get {
                return ResourceManager.GetString("Message_DataDeleted", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &quot;Нихао! Это Минг-бот, я помогу тебе сделать правильную транскрипцию слов. Выбери команду:&quot;, &quot;Нихао! Это Минг-бот, я помогу тебе правльно записать любое слово. Какое действие мне выполнить:&quot;, &quot;Нихао! Это Минг-бот, я могу рассказать тебе, как правильно произносится любое слово. С чего мне начать:&quot;.
        /// </summary>
        internal static string Message_Greetings {
            get {
                return ResourceManager.GetString("Message_Greetings", resourceCulture);
            }
        }
    }
}
