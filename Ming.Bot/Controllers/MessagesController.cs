﻿using System;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Floxdc.Ming.Bot.Infrastructure;
using Floxdc.Ming.Bot.Model;
using Floxdc.Ming.Bot.Resource;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using NLog;

namespace Floxdc.Ming.Bot.Controllers
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        [ResponseType(typeof(void))]
        public virtual async Task<HttpResponseMessage> Post([FromBody]Activity activity)
        {
            if (activity == null)
                return new HttpResponseMessage(HttpStatusCode.Accepted);

            switch (activity.GetActivityType())
            {
                case ActivityTypes.Message:
                    await Conversation.SendAsync(activity, MakeRoot);
                    break;
                //TODO: case ActivityTypes.ConversationUpdate:
                case ActivityTypes.ContactRelationUpdate:
                    await ContactRelationUpdate(activity);
                    break;
                case ActivityTypes.DeleteUserData:
                    await DeleteUserData(activity);
                    break;
                case ActivityTypes.Ping:
                    return new HttpResponseMessage(HttpStatusCode.OK);
                default:
                    Logger.Warn(Floxdc_Ming_Bot_Controllers_MessagesController.Critical_UnknownActivityType, activity.GetActivityType());
                    break;
            }

            return new HttpResponseMessage(HttpStatusCode.Accepted);
        }


        public async static Task ContactRelationUpdate(IContactRelationUpdateActivity activity)
        {
            if (activity.Action == "remove")
                return;

            var connector = new ConnectorClient(new Uri(activity.ServiceUrl));
            var reply = Replier.GetMessageReply(activity, Floxdc_Ming_Bot_Controllers_MessagesController.Message_Greetings.PickOne());
            await connector.Conversations.ReplyToActivityAsync(activity.Conversation.Id, reply.Id, reply);
            reply.Text = "test";
            await connector.Conversations.ReplyToActivityAsync(activity.Conversation.Id, reply.Id, reply);
        }


        private async static Task DeleteUserData(IActivity activity)
        {
            var connector = new ConnectorClient(new Uri(activity.ServiceUrl));

            var typingReply = Replier.GetTypingReply(activity);
            await connector.Conversations.ReplyToActivityAsync(activity.Conversation.Id, typingReply.Id, typingReply);

            //TODO
            var userId = activity.From.Id;
            
            var reply = Replier.GetMessageReply(activity, Floxdc_Ming_Bot_Controllers_MessagesController.Message_DataDeleted);
            await connector.Conversations.ReplyToActivityAsync(activity.Conversation.Id, reply.Id, reply);
        }


        private static IDialog<MingDialog> MakeRoot()
            => Chain.From((() => new MingDialog()))
                .Do(async (context, dialog) =>
                {
                    try
                    {
                        var typingReply = Replier.GetTypingReply(context);
                        await context.PostAsync(typingReply);

                        var completed = await dialog;
                    }
                    catch (FormCanceledException<MingDialog> ex)
                    {
                        LogLevel logLevel;
                        string reply;
                        if (ex.InnerException == null)
                        {
                            reply = string.Format(Floxdc_Ming_Bot_Controllers_MessagesController.Critical_FormCanceled,
                                ex.Last);
                            logLevel = LogLevel.Warn;
                        }
                        else
                        {
                            reply = Floxdc_Ming_Bot_Controllers_MessagesController.Critical_FormCanceledWithException;
                            logLevel = LogLevel.Fatal;
                        }

                        Logger.Log(logLevel, ex, ex.Message);
                        await context.PostAsync(reply);
                    }
                    catch (Exception ex)
                    {
                        Logger.Log(LogLevel.Fatal, ex, ex.Message);
                        await
                            context.PostAsync(
                                Floxdc_Ming_Bot_Controllers_MessagesController.Critical_FormCanceledWithException);
                    }
                });


        private static readonly ILogger Logger = LogManager.GetCurrentClassLogger();
    }
}